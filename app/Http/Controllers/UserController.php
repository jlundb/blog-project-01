<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Models\User;
use App\Http\Requests;

class UserController extends Controller
{
  public function index() {
    return response()->json(User::all());
  }

  public function show($id) {
    return response()->json(['name' => 'SHOW', 'state' => 'CA']);
  }

  public function store() {
    return response()->json(['name' => 'POST', 'state' => 'CA']);
  }

  public function destroy($id) {
    return response()->json(['name' => 'DELETE', 'state' => 'CA']);
  }
    //
}
