<?php

namespace App\Http\Controllers;

use App\Http\Models\Blogger;
use App\Http\Models\Comment;
use App\Http\Models\Post;
use App\Http\Models\PostDetail;
use Illuminate\Http\Request;
use App\Http\Requests;

class PostController extends Controller
{
  public function index() {
    return response()->json(Post::all());
  }

  public function show($id) {
    if(is_numeric($id) && $id > 0) {
      $post = Post::find($id);

      if(empty($post)) {
        $response = array('response_status'=>'not found');
      }  else {
        $post->blogger = Blogger::getBasicBlogger((integer)$post->blogger_id);
        $post->post_details = PostDetail::where('post_id','=',$id)->get();
        $post->comments = Comment::where('post_id','=',$id, 'and', 'parent_id','is',null)->get();
        $response = $post;
      }
    } else {
      $status_message = 'invalid parameter: '.$id;
      $response = array('response_status'=>$status_message);
    }

    return response()->json($response);
    //return (string)$response;
  }

  public function store() {
    return response()->json(Blogger::all());
  }

  public function destroy($id) {
    return response()->json(Blogger::all());
  }
    //
}
