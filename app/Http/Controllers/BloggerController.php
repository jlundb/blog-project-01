<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Models\Blogger;
use App\Http\Models\Post;
use App\Http\Requests;

class BloggerController extends Controller
{
  public function index() {
    return response()->json(Blogger::all());
  }

  public function show($id) {
      if(is_numeric($id) && $id > 0) {
        $blogger = Blogger::find($id);

        if(empty($blogger)) {
          $response = array('response_status'=>'not found');
        }  else {
          $blogger->posts = Post::where('blogger_id','=',$id)->get();
          $response = $blogger;
        }
      } else {
        $status_message = 'invalid parameter: '.$id;
        $response = array('response_status'=>$status_message);
      }

      return response()->json($response);

  }

  public function store() {
    return response()->json(Blogger::all());
  }

  public function destroy($id) {
    return response()->json(Blogger::all());
  }
    //
}
