<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $hidden = ['blogger_id','deleted'];

  protected $casts = [
      'blogger_id' => 'integer',
      'language_id'=> 'integer',
      'post_title' => 'string',
      'post_description' => 'string',
      'active' => 'boolean',
      'deleted' => 'boolean',
      'comments_enabled' => 'boolean',
      'created_at' => 'string',
      'updated_at'=> 'string'
  ];

}
