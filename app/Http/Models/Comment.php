<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $hidden = ['ip_addr'];
  protected $casts = [
      'post_id' => 'integer',
      'parent_id'=> 'integer',
      'commenter_name' => 'string',
      'commenter_text' => 'string',
      'likes' => 'integer',
      'dislikes' => 'integer',
      'ip_addr' => 'string'
  ];
}
