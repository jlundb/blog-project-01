<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  protected $hidden = ['created_at','updated_at'];
  protected $casts = [
      'full_path' => 'string',
      'relative_path'=> 'string',
      'image_type' => 'string',
      'image_size' => 'string',
      'image_name' => 'integer',
      'image_description' => 'string'
  ];
}
