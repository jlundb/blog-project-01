<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
  protected $casts = [
      'language_name' => 'string',
      'standard'=> 'string',
      'code' => 'string',
      'is_default' => 'boolean',
      'sequence' => 'integer'
  ];
}
