<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PostDetail extends Model
{
  protected $casts = [
      'post_id' => 'integer',
      'post_content'=> 'string',
      'is_image' => 'boolean',
      'image_id' => 'integer',
      'sequence' => 'integer'
  ];
}
