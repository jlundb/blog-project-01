<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Blogger extends Model
{
  protected $hidden = ['active'];
  protected $casts = [
      'first_name' => 'string',
      'last_name'=> 'string',
      'email' => 'string',
      'phone' => 'string',
      'active' => 'boolean',
      'status' => 'string',
      'created_at' => 'string',
      'updated_at'=> 'string'
  ];

  public static function getBasicBlogger($id) {
    if(is_numeric($id) && $id > 0) {
      $blogger_result_set = Blogger::where('id','=',$id)->get();
      $blogsRetrieved = count($blogger_result_set);
      if($blogsRetrieved !== 1) throw new Exception('The result did not return 1, returned: ');
      return $blogger_result_set[0];
    } else {
      throw new Exception(' is not a valid parameter');
    }

  }


}
