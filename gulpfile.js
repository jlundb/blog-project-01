var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.scripts([
        'open.js',
        'app.js',
        'routes/routes.js',
        'services/bloggerService.js',
        'services/postService.js',
        'controllers/mainCtrl.js',
        'controllers/blogEditCtrl.js',
        'controllers/blogCtrl.js',
        'controllers/postCtrl.js',
        'filters/filters.js',
        'close.js'
    ]);

});

elixir(function(mix) {
  mix.styles([
    'clean-blog.css',
    'general.css'
  ]);
});

elixir(function(mix) {
    mix.less('app.less');
});
