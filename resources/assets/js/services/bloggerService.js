app.service('bloggerService', ['$http','$q', function($http, $q) {
  var API_URL = '/api/blogger';

  this.getBlogger = function(id) {
    var bloggerApiUrl;
    if(!id) bloggerApiUrl = API_URL;
    else if ((typeof id) === 'number' && id > 0) bloggerApiUrl = API_URL + '/' + id;
    else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }

    return $http.get(bloggerApiUrl);
  };

  this.saveBlogger = function(blogger) {
    if(!blogger || (typeof blogger) !== 'object') return;
    var method; //

    if(!blogger.id) method = 'POST';
    else if((typeof blogger.id) === 'number' && bloger.id > 0) method = 'PUT';
    else {
      console.log('The data integrity of the parameter is compromised');
      console.log(blogger);
      return;
    }

    $http({
            method: method,
            url: API_URL
          }, data)
    .success(function (data) {
      return data;
    })
    .error(function (data) {
      console.log(data);
      return;
    });

  };

  this.deleteBlogger = function(id) {
    if(id && (typeof id) === 'number') {
      $http.delete(API_URL + '/' + id).then(function(response) {
        return response.data;
      }, function(error) {
        console.log(error);
      });
    } else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }
  };

}]);
