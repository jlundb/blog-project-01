app.service('postService', ['$http','$q', function($http, $q) {
  var API_URL = '/api/post';

  this.getPost = function(id) {
    var postApiUrl;
    console.log("id", typeof id, id);
    if(!id) postApiUrl = API_URL;
    else if ((typeof id) === 'number' && id > 0) postApiUrl = API_URL + '/' + id;
    else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }
    return $http.get(postApiUrl);
  };

  this.savePost = function(post) {
    if(!post || (typeof post) !== 'object') return;
    var method;

    if(!post.id) method = 'POST';
    else if((typeof post.id) === 'number' && bloger.id > 0) method = 'PUT';
    else {
      console.log('The data integrity of the parameter is compromised');
      console.log(post);
      return;
    }

    $http({
            method: method,
            url: factory.API_URL
          }, data)
    .success(function (data) {
      return data;
    })
    .error(function (data) {
      console.log(data);
      return;
    });

  };

  this.deletePost = function(id) {
    if(id && (typeof id) === 'number') {
      $http.delete(API_URL + '/' + id).then(function(response) {
        return response.data;
      }, function(error) {
        console.log(error);
      });
    } else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }
  };

}]);
