app.config(function($routeProvider) {
    $routeProvider
    .when('/home', {templateUrl:'partials/home.html'})
    .when('/blog', {templateUrl:'partials/blog.html', controller: 'blogCtrl'})
    .when('/about', {templateUrl:'partials/about.html'})
    .when('/contact',{templateUrl:'partials/contact.html'})
    .when('/blog-edit',{templateUrl:'partials/blog-edit.html', controller: 'blogEditCtrl'})
    .when('/post/:id/', {templateUrl:'partials/post.html', controller: 'postCtrl'})
    .otherwise({redirectTo:'/home'});
  });
