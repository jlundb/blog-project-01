app.controller('mainCtrl', function($scope, $location) {

  $scope.header = 'partials/navbar.html';
  $scope.footer = 'partials/footer.html';
  $scope.loading = true;

  $scope.setRoute = function(route) {
    if(route) $location.path(route);
  };

  $scope.isLoading = function() {
    return $scope.loading;
  };
  //
  $scope.setLoading = function(boolean) {
    if((typeof boolean) === 'boolean') $scope.loading = boolean;
  };

  $scope.clone = function(source) {
    return angular.copy(source);
  };

  $scope.equals = function(o1, o2) {
    return angular.equals(o1, o2);
  };

  $scope.isArray = function(value) {
    return angular.isArray(value);
  };

  $scope.isObject = function(value) {
    return angular.isObject(value);
  };

  $scope.splitSecondsFromDate = function(date) {
    if(typeof date === 'string')
      return date.split(' ')[0];
  };



});
