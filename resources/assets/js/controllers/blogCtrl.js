app.controller('blogCtrl', function($scope, bloggerService) {
  $scope.setLoading(true);

  $scope.data = {
      authorWithPosts: null,
      page: 1,
      postPerPage: 7
  };

  $scope.originalState = $scope.clone($scope.data);

  bloggerService.getBlogger(2).then(
    function(response) {
      $scope.setLoading(false);
      $scope.setAuthorWithPosts(response.data);
    }, function(error) {
       console.log("error", error);
    });

  $scope.getCurrentPage = function() {
    return $scope.data.page;
  };

  $scope.getPostsPerPage = function() {
    return $scope.data.postPerPage;
  };

  $scope.getAuthorWithPosts = function() {
    return $scope.data.authorWithPosts;
  };

  $scope.setAuthorWithPosts = function(authorWithPosts) {
    if($scope.isObject(authorWithPosts))
      $scope.data.authorWithPosts = authorWithPosts;
  };

  $scope.getAuthorName = function() {
    return $scope.data.authorWithPosts.first_name + " " + $scope.data.authorWithPosts.last_name;
  };

  $scope.setPage = function(pageNr) {
    if(typeof pageNr === 'number' && pageNr > 0)
      $scope.data.page = pageNr;
  };

  $scope.incrementPage = function(boolean) {
    if(typeof boolean === 'boolean') {
      if(boolean) $scope.data.page++;
      else if($scope.data.page > 1) $scope.data.page--;
    }
  };

  $scope.canGoLeft = function() {
    return $scope.data.page !== 1;
  };

  $scope.canGoRight = function() {
    var posts = $scope.data.authorWithPosts ? $scope.data.authorWithPosts.posts : null;
    if($scope.isArray(posts)) {
      return (posts.length > ($scope.data.postPerPage * $scope.data.page));
    } else return false;
  };

  $scope.fetchBloggerData = function() {
    bloggerService.getBlogger(3).then(
      function(response) {
        $scope.setLoading(false);
        $scope.setAuthorWithPosts(response.data);
      }, function(error) {
         console.log("error", error);
      });
  };

});
