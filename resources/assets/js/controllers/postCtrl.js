app.controller('postCtrl', ['$scope','$location','$routeParams', 'postService', function($scope, $location, $routeParams, postService) {
  console.log($routeParams);
  console.log("Scope data",$scope.data);
  $scope.setLoading(true);
  $scope.data = {
    urlParameters: $routeParams,
    post: null
  }

  $scope.getPost = function() {
    if($scope.data) return $scope.data;
  };

  $scope.setPost = function(data) {
    $scope.data = data;
  };

  $scope.getAuthorName = function() {
    if($scope.data && $scope.data.blogger)
      return $scope.data.blogger.first_name + ' ' + $scope.data.blogger.last_name;
  };

  $scope.makeComment = function(text) {
    console.log(text);
  };

  postService.getPost(parseInt($scope.data.urlParameters.id))
    .then(
      function(response) {
        console.log(response);
        $scope.setPost(response.data);
        $scope.setLoading(false);
      }, function(error) {
        console.log(error);
      });


}]);
