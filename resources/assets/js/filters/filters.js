app.filter('pagination', function() {
  return function(elements, currPage, numOfPages) {
    if(elements && elements.length) {
      var begin = (currPage - 1) * numOfPages;
      var end = currPage * numOfPages;
      return elements.slice(begin, end);
    }
  };
});
