(function() {
'use strict';

var app = angular.module('blog', ['ngResource','ui.bootstrap','ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
    .when('/home', {templateUrl:'partials/home.html'})
    .when('/blog', {templateUrl:'partials/blog.html', controller: 'blogCtrl'})
    .when('/about', {templateUrl:'partials/about.html'})
    .when('/contact',{templateUrl:'partials/contact.html'})
    .when('/blog-edit',{templateUrl:'partials/blog-edit.html', controller: 'blogEditCtrl'})
    .when('/post/:id/', {templateUrl:'partials/post.html', controller: 'postCtrl'})
    .otherwise({redirectTo:'/home'});
  });

app.service('bloggerService', ['$http','$q', function($http, $q) {
  var API_URL = '/api/blogger';

  this.getBlogger = function(id) {
    var bloggerApiUrl;
    if(!id) bloggerApiUrl = API_URL;
    else if ((typeof id) === 'number' && id > 0) bloggerApiUrl = API_URL + '/' + id;
    else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }

    return $http.get(bloggerApiUrl);
  };

  this.saveBlogger = function(blogger) {
    if(!blogger || (typeof blogger) !== 'object') return;
    var method; //

    if(!blogger.id) method = 'POST';
    else if((typeof blogger.id) === 'number' && bloger.id > 0) method = 'PUT';
    else {
      console.log('The data integrity of the parameter is compromised');
      console.log(blogger);
      return;
    }

    $http({
            method: method,
            url: API_URL
          }, data)
    .success(function (data) {
      return data;
    })
    .error(function (data) {
      console.log(data);
      return;
    });

  };

  this.deleteBlogger = function(id) {
    if(id && (typeof id) === 'number') {
      $http.delete(API_URL + '/' + id).then(function(response) {
        return response.data;
      }, function(error) {
        console.log(error);
      });
    } else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }
  };

}]);

app.service('postService', ['$http','$q', function($http, $q) {
  var API_URL = '/api/post';

  this.getPost = function(id) {
    var postApiUrl;
    console.log("id", typeof id, id);
    if(!id) postApiUrl = API_URL;
    else if ((typeof id) === 'number' && id > 0) postApiUrl = API_URL + '/' + id;
    else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }
    return $http.get(postApiUrl);
  };

  this.savePost = function(post) {
    if(!post || (typeof post) !== 'object') return;
    var method;

    if(!post.id) method = 'POST';
    else if((typeof post.id) === 'number' && bloger.id > 0) method = 'PUT';
    else {
      console.log('The data integrity of the parameter is compromised');
      console.log(post);
      return;
    }

    $http({
            method: method,
            url: factory.API_URL
          }, data)
    .success(function (data) {
      return data;
    })
    .error(function (data) {
      console.log(data);
      return;
    });

  };

  this.deletePost = function(id) {
    if(id && (typeof id) === 'number') {
      $http.delete(API_URL + '/' + id).then(function(response) {
        return response.data;
      }, function(error) {
        console.log(error);
      });
    } else {
      console.log('Parameter: ' + id + ' is not valid');
      return;
    }
  };

}]);

app.controller('mainCtrl', function($scope, $location) {

  $scope.header = 'partials/navbar.html';
  $scope.footer = 'partials/footer.html';
  $scope.loading = true;

  $scope.setRoute = function(route) {
    if(route) $location.path(route);
  };

  $scope.isLoading = function() {
    return $scope.loading;
  };
  //
  $scope.setLoading = function(boolean) {
    if((typeof boolean) === 'boolean') $scope.loading = boolean;
  };

  $scope.clone = function(source) {
    return angular.copy(source);
  };

  $scope.equals = function(o1, o2) {
    return angular.equals(o1, o2);
  };

  $scope.isArray = function(value) {
    return angular.isArray(value);
  };

  $scope.isObject = function(value) {
    return angular.isObject(value);
  };

  $scope.splitSecondsFromDate = function(date) {
    if(typeof date === 'string')
      return date.split(' ')[0];
  };



});

app.controller('blogEditCtrl', function($scope) {
  console.log("Running Blog edit controller");
});

app.controller('blogCtrl', function($scope, bloggerService) {
  $scope.setLoading(true);

  $scope.data = {
      authorWithPosts: null,
      page: 1,
      postPerPage: 7
  };

  $scope.originalState = $scope.clone($scope.data);

  bloggerService.getBlogger(2).then(
    function(response) {
      $scope.setLoading(false);
      $scope.setAuthorWithPosts(response.data);
    }, function(error) {
       console.log("error", error);
    });

  $scope.getCurrentPage = function() {
    return $scope.data.page;
  };

  $scope.getPostsPerPage = function() {
    return $scope.data.postPerPage;
  };

  $scope.getAuthorWithPosts = function() {
    return $scope.data.authorWithPosts;
  };

  $scope.setAuthorWithPosts = function(authorWithPosts) {
    if($scope.isObject(authorWithPosts))
      $scope.data.authorWithPosts = authorWithPosts;
  };

  $scope.getAuthorName = function() {
    return $scope.data.authorWithPosts.first_name + " " + $scope.data.authorWithPosts.last_name;
  };

  $scope.setPage = function(pageNr) {
    if(typeof pageNr === 'number' && pageNr > 0)
      $scope.data.page = pageNr;
  };

  $scope.incrementPage = function(boolean) {
    if(typeof boolean === 'boolean') {
      if(boolean) $scope.data.page++;
      else if($scope.data.page > 1) $scope.data.page--;
    }
  };

  $scope.canGoLeft = function() {
    return $scope.data.page !== 1;
  };

  $scope.canGoRight = function() {
    var posts = $scope.data.authorWithPosts ? $scope.data.authorWithPosts.posts : null;
    if($scope.isArray(posts)) {
      return (posts.length > ($scope.data.postPerPage * $scope.data.page));
    } else return false;
  };

  $scope.fetchBloggerData = function() {
    bloggerService.getBlogger(3).then(
      function(response) {
        $scope.setLoading(false);
        $scope.setAuthorWithPosts(response.data);
      }, function(error) {
         console.log("error", error);
      });
  };

});

app.controller('postCtrl', ['$scope','$location','$routeParams', 'postService', function($scope, $location, $routeParams, postService) {
  console.log($routeParams);
  console.log("Scope data",$scope.data);
  $scope.setLoading(true);
  $scope.data = {
    urlParameters: $routeParams,
    post: null
  }

  $scope.getPost = function() {
    if($scope.data) return $scope.data;
  };

  $scope.setPost = function(data) {
    $scope.data = data;
  };

  $scope.getAuthorName = function() {
    if($scope.data && $scope.data.blogger)
      return $scope.data.blogger.first_name + ' ' + $scope.data.blogger.last_name;
  };

  $scope.makeComment = function(text) {
    console.log(text);
  };

  postService.getPost(parseInt($scope.data.urlParameters.id))
    .then(
      function(response) {
        console.log(response);
        $scope.setPost(response.data);
        $scope.setLoading(false);
      }, function(error) {
        console.log(error);
      });


}]);

app.filter('pagination', function() {
  return function(elements, currPage, numOfPages) {
    if(elements && elements.length) {
      var begin = (currPage - 1) * numOfPages;
      var end = currPage * numOfPages;
      return elements.slice(begin, end);
    }
  };
});

})();

//# sourceMappingURL=all.js.map
