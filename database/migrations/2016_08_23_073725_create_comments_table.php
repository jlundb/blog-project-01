<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function(Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id')->unsigned();
          $table->integer('post_id')->unsigned();
          $table->integer('parent_id')->nullable()->unsigned();
          $table->string('commenter_name');
          $table->text('commenter_text');
          $table->integer('likes')->unsigned();
          $table->integer('dislikes')->unsigned();
          $table->string('ip_addr')->nullable();
          $table->foreign('post_id')->references('id')->on('posts');
          $table->foreign('parent_id')->references('id')->on('comments');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
