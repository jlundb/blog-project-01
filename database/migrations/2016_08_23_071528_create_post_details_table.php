<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_details', function(Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id')->unsigned();
          $table->integer('post_id')->unsigned();
          $table->text('post_content');
          $table->boolean('is_image');
          $table->integer('image_id')->nullable()->unsigned();
          $table->integer('sequence');
          $table->foreign('post_id')->references('id')->on('posts');
          $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_details');
    }
}
