<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function(Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id')->unsigned();
          $table->integer('blogger_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->string('post_title');
          $table->string('post_description').nullable();
          $table->boolean('active');
          $table->boolean('deleted');
          $table->boolean('comments_enabled');
          $table->integer('sequence');
          $table->timestamps();
          $table->foreign('blogger_id')->references('id')->on('bloggers');
          $table->foreign('language_id')->references('id')->on('languages');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
