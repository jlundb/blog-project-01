<?php

use Illuminate\Database\Seeder;

class BloggerTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      $now = date('Y-m-d H:i:s');

      DB::table('bloggers')->insert(
        array(
          array(
            'first_name' => 'Anne-Marie',
            'last_name' => 'Fuglevik Sagen',
            'email'=>'amfs@hotmail.com',
            'phone'=> '+47 000000000',
            'active'=> 1,
            'status'=> 'active',
            'created_at' => $now,
          ),
          array(
            'first_name' => 'Jon Walter',
            'last_name' => 'Lundberg',
            'email'=>'jwl@hotmail.com',
            'phone'=> '+47 000000000',
            'active'=> 1,
            'status'=> 'active',
            'created_at' => $now,
          ),
        )
      );
    }
}
