<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('languages')->insert(
          array(
            array(
              'language_name' => 'English',
              'standard' => 'ISO 639-1',
              'code'=>'en',
              'is_default'=> 0,
              'sequence'=>1
            ),
            array(
              'language_name' => 'Norsk',
              'standard' => 'ISO 639-1',
              'code'=>'no',
              'is_default'=> 1,
              'sequence'=> 0
            ),
          )
        );
    }
}
